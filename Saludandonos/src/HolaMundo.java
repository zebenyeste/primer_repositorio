//Programa: hola_mundo.java
//Autor: Zeben Yeste
//Versión: 1.0
//Fecha: 09/05/2013
//Hora: 10:27 a.m.
//Descripción: muestra una cadena de texto por la salida estándar.

public class HolaMundo {
	public static void main(String[] args){
		System.out.println("Hola mundo remoto desde Bitbucket.com");
	}
}

//Comentario final poco productivo.
